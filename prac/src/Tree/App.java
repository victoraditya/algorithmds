package Tree;

public class App {

	public static void main(String[] args) {
		
		Node root = null;
		
		int[] keys = { 15, 10, 20, 8, 12, 16, 25 };
		
		for(int key : keys) {
			root = insertNodeBST(root, key);
		}

		
		
		inorder(root);

	}
	
	private static void inorder(Node root) {
		if(root==null) return;
		
		inorder(root.left);;
		System.out.println(root.getData());
		inorder(root.getRight());
		
	}

	public  static Node insertNodeBST(Node root, int key) {
		if (root == null) {
			return new Node(key);
		}
		if (key<root.getData()) {
			root.left = insertNodeBST(root.left, key);
		}else if(key >= root.data){
			root.right = insertNodeBST(root.right, key);
		}
		
		return root;
		
		
	}

}
